FROM golang:1.15-alpine

RUN apk add bash ca-certificates git gcc g++ libc-dev

ENV GO111MODULE=on

WORKDIR $GOPATH
